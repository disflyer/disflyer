### Hi there <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25px">

<!-- ![visitors](https://visitor-badge.glitch.me/badge?page_id=dislfyer.dislfyer) -->

Hi, I'm Dylan, a Frontend Developer 🚀 from China, 🏠 base in shanghai. Now working in Tesla.

<br/>
<br/>

📊 **This Week I Spent My Time On:**


<!--START_SECTION:waka-->

```text
TypeScript                          ███████████████████░░░░░░  76.73%
Docker                              ██░░░░░░░░░░░░░░░░░░░░░░░  8.66%
JSON                                █░░░░░░░░░░░░░░░░░░░░░░░░  4.33%
Nginx configuration file            ▓░░░░░░░░░░░░░░░░░░░░░░░░  2.79%
Other                               ▓░░░░░░░░░░░░░░░░░░░░░░░░  2.38%
```

<!--END_SECTION:waka-->

<!--
**About Me:**
 -->
